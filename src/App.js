import React, {useState, useEffect} from 'react';
import './App.css';
import axios from "axios";
import Modal from './Components/Modal';
import ModalProvider from './Contexts/ModalProvider';
import get from 'lodash/get'

const App = () => {

    const [value, setValue] = useState('');
    const [answer, setAnswer] = useState(null);

    useEffect(() => {
        document.addEventListener('selectionchange', () => {
            if (get(document.getSelection(),'focusNode.data')) {
                if (get(document.getSelection(),'focusNode.data') !== value) {
                    setValue(get(document.getSelection(),'focusNode.data'))
                    getAnswerAction(get(document.getSelection(),'focusNode.data'))
                }
            }
            document.addEventListener('copy', (event) => {
                const selection = document.getSelection();
                if (get(selection, 'extentNode.data')) {
                    if (get(selection, 'extentNode.data') !== value) {
                        setValue(get(selection, 'extentNode.data'))
                        getAnswerAction(get(selection, 'extentNode.data'))
                    }
                }
            });
        });

    }, []);


    const getAnswerAction = (text) => {
        axios.post(`http://alumni.andqxai.uz/api/answers/find-by-quiz`, {
            quizContent: text
        }).then(data => {
            setAnswer(data.data);
        }).catch(e => {
            setAnswer('Xatolik yuz berdi');
        })
    }

    return answer ? (
        <ModalProvider>
            <Modal {...{answer, setAnswer}}/>
        </ModalProvider>

    ) : null
}

export default App;
